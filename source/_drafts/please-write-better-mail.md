---
title: 'please, write better mail'
date: 2019-12-06 17:07:26
thumbnail: https://images.pexels.com/photos/3059854/pexels-photo-3059854.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260
tags:
	- Comumnication
	- Professional
	- Rambling
	- Indonesia
category:
	- [Thought]
	- [Indonesia]
---

Kemarin saya mendapatkan undangan untuk mengisi di salah satu kampus di Bandung, teman saya yang mengundang. Oké, teman yang mengundang, tapi saya tetap meminta undangan resmi dari pihak laboratorium yang mengundang. "Biar resmi mintanya ke kantor”, kata saya.

<!--more-->

Well, undangan pun dikirim, via email. Membaca emailnya, saya langsung _ngakak_, dan saya kirimkan ke teman saya. 

me: “This bad?” 

he: “Worse” 

me: :man_facepalming: 

Adek-adekku anak kuliahan, ketahuilah bahwa kemampuan kalian menulis email menunjukkan sampai mana kemampuan komunikasi tekstual kalian. Serius. Kalau kalian tidak bisa menulis email secara serius seperti kalian sedang menulis surat resmi, kalian ndak akan bisa menguasai cara komunikasi dengan _chatting_ app yang akan mendapatkan respon segera dari dosen.

Kasus ini bukan hanya terjadi di kampus, tapi di dunia kerja juga. Terutama anak-anak baru lulus ( _fresh grad_ ) yang penuh dengan semangat dan pengalaman organisasi tapi minim pengalaman kerja rodi.

Saya sering harus mengingatkan masalah kata-kata yang digunakan, apa saja yang ditulis, urut-urutan menulis, dan masalah attachment di email yang mereka kirimkan. Lebih dari itu, saya juga masih harus mengingatkan apa saja yang _better_ menggunakan email dan yang _better_ menggunakan telepon.

Tidak ada _chat app_? Anggap saja telepon itu gabungan telepon dan chatting. Require immediate attendance kan?

Budaya komunikasi tekstual kita memang tidak terbiasa dengan menggunakan email, yang kita kenal pertama bukan email dan mailing list, tetapi SMS. Ingat zaman SMS rame sekitar tahun 2006 sampai 2012’an? Kita terbiasa dengan pesan singkat dan pesan berantai. Hal ini secara tidak langsung mengurangi kemampuan kita untuk menyusun informasi secara lengkap dan formal, karena kita biasa terbatas oleh batasan karakter dan budget pulsa.

Tapi apa ya harus terus begitu? Twitter saja akhirnya update ke 280 karakter karena 140 itu membuat semua orang membuat thread hanya untuk menyampaikan "aku lelah ingin pulang”. Pembatasan itu sekarang sudah ndak ada. Seharusnya kita makin bisa menggunakan teknologi ini dengan benar.

Beberapa orang bilang saya saklek sih untuk urusan begini. Well, saya berpegang pada prinsip bahwa sifat email adalah semi-publik, bukan murni private. Email bisa digunakan sebagai bukti otentik atas kejadian terkait dengan email tersebut. _So, anything important, record it on e-mail_.

Dan email tidak memaksa setiap orang untuk mengecek dan menjawabnya secara langsung. Ini yang paling penting. Kita tidak berhak selalu meminta respon secepatnya dari orang lain, karena kita tidak selalu tahu apa yang sedang dia kerjakan. Budaya _chatting app_ saat ini memang lumayan berbahaya karena orang-orang cenderung menginginkan respon yang cepat atas pesannya. Is that bad? Definitely. Anak-anak yang nge-_chat_ dosennya dengan awalan "P” atau "Test” _instead of_ "Selamat pagi, Pak A, perkenalkan saya B” atau "Selamat siang Pak B, saya ada perlu bertemu bapak untuk melakukan bimbingan, kapan kira-kira bapak ada di kampus dan bersedia?” adalah hasil dari ingin respon yang cepat tersebut. _Don’t blame your docent lah_ kalo mereka ga jawab karena malas.

_Use chat app and email accordingly_. Diskusi lepas yang mungkin perlu langsung tek-tok bisa dilakukan dengan chat app. Apalagi dengan teman sebaya. Saya juga ga saklek-saklek amat urusan begitu. Mailing list saya zero tapi WAG saya banyak sekali. 

Tapi tolong, kalau mau kirim sesuatu yang formal dan penting ke saya, termasuk undangan, pakai email yang baik dan benar ya, ga usah formal-formal banget. Yang penting ketika saya baca saya ngerti maksudnya. Ga bikin bingung gitu. Seperti email yang isinya hanya "FYI” tanpa ada diskusi offline atau via call sebelumnya, atau bukan reply dari email yang lain. Mana saya tahu maksud anda?

Kalo mau nyantai pake telegram saja. :sweat_smile:

---

Photo by **[Roman Koval](https://www.pexels.com/@kovalrk?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** from **[Pexels](https://www.pexels.com/photo/shalow-focus-photography-of-mailed-letters-3059854/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**