---
title: About
date: 2019-11-23 20:07:57
---

Hi! The name’s Muhammad, I am an IT guy, professionally I am an [Analyst](https://mutsuenka.gitlab.io/categories/Analysis) that [commute on motorcycle](https://mutsuenka.gitlab.io/categories/motard). Casually, I’m just like those not-so-geek person that like [books](https://mutsuenka.gitlab.io/categories/book-review), [games](https://mutsuenka.gitlab.io/categories/games) and playing around with [sword](https://mutsuenka.gitlab.io/categories/HEMA).

I can be contacted directly through [email](mailto:sunamuhardi@outlook.com).

See you on [fosstodon](https://fosstodon.org) for casual networking!

—Muhammad