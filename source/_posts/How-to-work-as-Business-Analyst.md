---
title: How I work as Business Analyst
toc: true
thumbnail: https://images.unsplash.com/photo-1454165804606-c3d57bc86b40?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80
sidebar:
  left:
    sticky: true
widgets:
  - type: toc
    position: left
date: 2016-12-22 16:57:51
tags:
	- Requirement
	- Business Analyst
	- Professional
category:
	- Analyst Life
	- Requirement Engineering
---

I have been an analyst for almost 8 projects within my first 2 years working as Business Analyst. Harsh truth, especially when you need to do your job with minimum supervising, which left you alone to figure out how to make things work.

<!--more-->

And analyst _is_ a front line. They do things wrong, everything goes wrong from cost estimation to project timeline and from developer tasks which impact final product. So please be real cautious when you’re doing your job as analyst, especially on projects.

After those projects, I found myself repeating same pattern over and over again. These pattern might be already described at several book or article of Requirement Engineering, but to be honest I don’t really pay attention to those book. I did read some of them, but there are some condition in real working world that just doesn’t work the same as those book, where you need to improvise all you can to ensure the safety ofthe project.

So, here’s my how-to. This is personal how-to, which means, it is the one best working for me. I hope the pattern that I found here make sense and works for you too!

## Business problem first

One thing that somehow missed by many people when building an app is, what will this application serve? What will it solve? What the use of feature in this application that benefit the business. The business? Yes, the business is the key. 

Understanding Business Requirement is important so that you can understand what they need, which not necessarily what they want.

Client tend to make requests, but always tight on the budget, both money and time. So you need to make sure you deliver thing they need that solves their problem. And to be honest here, most of the **want** are out of their budget anyway.

Understanding business requirements that drive the development of this application will also help you adjust the implementation of business process in your application. You can strip several process that is fancy but doesn’t affect the current business. You can arrange priorities better. With deep understanding of the problem they want to solve, you will get better at negotiations. Because you can arrange how the solution is implemented better.

And don’t forget, budget and launch date is a part of business problem too.

## Drive them into right solution

There are many PM and BA consider saying “yes” to each request asked by client. Sometimes you need to do that because those are exactly what client need, but “sometimes” means “not everytime”. Thus, **not all** client’s request must be taken directly. You need to make a clear line between feature they need, and feature that raise the price without real benefit.

I don’t hate fancy stuff, but I hate unnecessary stuff. If they need sandwhich for breakfast than sandwhich it is, I don’t need to go order a complete wedding cake for breakfast, even though I might want to do that – which I don’t. They have different usage, even though they are both food all the same.

Some fancy might be needed, depending on the requirements. For example, they have need to centralise their documents and following a strict business process in doing their work. I might think that everything shall be entered via website, but when I see how they work daily, I might decide that there is no need for the web entry form and just go with the document control app, which saves a lot of money by cutting the process from maintaining input form and print out process, into single process of maintaining files.

So, right after they finishtlking about how their business works, take a deep breath, analyse them, and start steering your client into the planned solution direction. Don’t cut everything but don’t be a yes man either. keep it balanced, you really need to understand thing they need and things they simply want.

## Communication and Approval

Don’t guess _anything_. This is their money, they have the right to know what they will get. So, whenever you feel confused about one or two requirements, keep asking and confirming your client. Don’t do anything until they confirm. Keep critical decision (not regarding to your team) on them. Because again, this is their money.

Also, you need approval. For every discussion, every changes, initial requirements, mock-ups, etc, there must be an approval. You don’t need to run by them everyday asking signatures, an email confirmation is good enough for confirmation. But don’t forget to sign those SRS’s.

## Discuss with your developer

This is important part. Sometimes our ego  and feel of experienced is too high, we forgot to ask those who will  work on this application. Especially if you have works on lot of project and has technical background in software industry. But it is worthy to  ask your developer, so you can communicate client with more precise  answer instead of guess.

Developer team can help you find out how  to implement the application. Software architect can help you find out  what architecture is best for the solution. Discussion with developer  team can becomes a powerful evidence to talk with client, instead of  blabbering things about business process – which they own. Not a great  plan.

---

Next time, I will talk more about practical approach of requirement assessment, all things start with abstract and mentality. For now, following those points above have made my life easier. Both in managing requirements, or managing people around the requirements.

