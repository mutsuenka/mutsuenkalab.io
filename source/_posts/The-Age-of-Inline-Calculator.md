---
title: The Age of Inline Calculator
toc: true
date: 2019-12-02 16:35:07
tags:
	- Software
category:
	- Tools
---

We are mostly use Spreadsheet to do some functional calculation and use standard calculator to perform basic calculation. The problem arise when you need to do functional calculation as casual as basic calculation. If you are amid to programming, you will fine that an ability to write down your calculation inline, using functions and variables as the program calculate automatically for you is _satisfying_. And that, is not weird idea anymore.

<!--more-->

I am exposed to this kind of application when I am switching to Mac for 6 monts (my mac died out after that). I used Numi, a paid inline calculator that does very good, design-wise and function-wise. I calculate and plan almost everything related to numbers using that application. 

When I go back using my old Windows machine, I am stunned by how that application has hooked me. I searched for the alternative of the app (thanks, alternativeto.net) and found a bunch of alternatives. _A Bunch_. Numbers that put me on the shame. How long have I’ve been on this IT world and why now?

So, there are a lot of alternatives, the most popular one? SpeedCrunch. That is not weird, considering it is open source and available on all major platform: Windows, Mac and Linux. So it is a universal language for all people using computer.

What I complain about Speedcrunch is the interface. Because I first know this kind of app by Numi, I use the standard UX of Numi to judge plain-text calculator. Numi using side by side interface, means that your result is in the right of your formula.

But I still use Speedcrunch in Linux since there are currently no better alternatives for plain-text calculator there. 

At Mac, Numi’s rival might be Soulver. Soulver is great. Definitely great. It is more "human” than any other plain-text calculator I’ve ever used. However, it is Windows we are talking about.

So, I decided to give OpalCalc a try. One of the best decision in my life. 

OpalCalc might not as clean as Numi—UI-wise—but it definitely had the almost same functionality, or might be better. OpalCalc looks like an old app, but it is the best I’ve ever seen, because I can access everything right in that application. For examples, the units. The documentation is good I need to cry. And also, it is donationware. 

I mean, if you are using the free, at least you can calculate for 5 rows. Not a bad deal. Since the calculation you do rarely exceed 5 rows.

But if you are starting to become accustomed to this kind of calculation, when you can forget about the cell position to do calculation. You kinda need more, and the creator only charge a few bucks for this app. A definitely fair deal. 

I purchased this application and now can’t do calculation but with it. Here’s an example of the calculation on the project I casually write on opalcalc.

![OpalCalc for Simple Project Calculation](https://live.staticflickr.com/65535/49167340511_56ea7fc53b_o_d.png)

Sorry for my bad naming on variables, but since this is a casual calculation that is not yet final, I just wrote it down there. There is Microsoft Project for this kind of calculation, of course. I just showing you how daily calculation can made easy by using this application.

So, for any of you that are bored and can’t bear with spreadsheet for every little thing, and are lazy to use the calculator because it quite, clicky, then these plain-text calculator are perfect for you!

You can use Speedcrunch, Numi, Soulver, or OpalCalc! Tell me if there are other good alternatives, I’ll definitely check them. Kudos for the guy that have thought about this application. And be sure to support them by buying the app if they are premium or freemium. I think that all users might be accustomed to programming and know how rewarding it is to know that people are using, and are willing to pay for your program because of the use.

To keep you from distraction, I give you the link at the end XD.

| App Name    | Link                                        |
| ----------- | ------------------------------------------- |
| OpalCalc    | https://www.skytopia.com/software/opalcalc/ |
| Numi        | https://numi.app/                           |
| SpeedCrunch | https://speedcrunch.org/                    |
| Soulver     | https://www.acqualia.com/soulver/           |

P.S.

Numi website is showing that it is still free. I was using Numi by Setapp so I thought it was paid. Better got them licensed before it going paid app :sweat_smile:.