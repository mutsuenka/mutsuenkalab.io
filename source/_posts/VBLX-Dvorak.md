---
title: VBLX-Dvorak
toc: true
date: 2015-11-03 14:49:49
tags: 
	- Dvorak
	- Personal Project
category: keyboard & layout
thumbnail: https://images.unsplash.com/photo-1573867975080-15a3d9445345?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80
sidebar:
    left:
        sticky: true
widgets:
    -
        type: category
        position: left
---

I was playing around with Dvorak, and then got guided to Programmer Dvorak. While Dvorak layout is nice already, Programmer Dvorak is nicer, because it put symbols above numbers (you need to press shift to type numbers, instead of shift to type symbol). I use this for awhile, and I have definitely no problem with the symbol X number swapping. <!--more--> 

I have problem with Dvorak, especially with placement of some keys.

My mind was provoked when I see Capewell Dovorak layout. he swapped a lot of letter, and by a lot I really mean _a lot_. Capewell Dvorak swap A and O, and put ZXCV back into their QWERTY position. While it is nice since it eases me in doing al tasks related to copy-paste, one thing I know for sure when I started to type using Capewell Dvorak:

It destroys my flow in typing.

I use Dvorak for its fluidity and comfort. If that’s broken then there will be no point in switching into Dvorak at all.

So I going back to review my key placement, and found that only 4 replacement will benefit me in typing Indonesian and English all alike. They are:

**V** in **X** position
**B** in **V** position
**L** in **B** position
**X** in **L** position

ooh, and don’t forget that **I** and **U** swapping!

I have tried the **O** and **A** swapping. It was nice … for a brief moment. I graduated for combination between them and consonant letter. But combination of O and other vocal almost all ended in failure and my wpm dropped drasticaly. And it was different to when I switch from QWERTY to DVORAK. This event enlighten me that it is really hard to change your habit partially. It ended destroyed your flow entirely. The best practice is still to change the habit entirely and thoroughly.

Note: I don’t really know whether this apply to reallife as well, I talk as typing and muscle memory, where rhythym is really important.

So, overall, my layout become like this:

![](https://c1.staticflickr.com/3/2817/33402951634_0476016095_c_d.jpg)

Keys in green is compose key—with some being dead key— that can be triggered by pressing AltGr or Ctrl+Alt. This is definitely helpful and I bet all of you European use these all the time. I took all the symbols placement from Programmer Dvorak, created by Roland Kauffman[^1]. Chevron is the one I like best and I just found by using Programmer Dvorak. It is “«” and “»” symbol. I use it in many diagram as closure since they are prettier than << and >>.

This layout is the last enhancement after experimenting and the only one that doesn’t destroy my rhythym. I can gain my old Dvorak speed (>85 wpm) in a week by learning and developing this layout.

I call this layout VBLX Dvorak for now. This name comes from letter that get replaced, minus the vocal swap. This layout is still in development to become totally different codes. I still try to analyse most common symbols used in my language and foreign language I spoke.

At the end, you may ask,

>   What did you get from this? Are you even typing faster?

My answer is,

>   I type more comfortable, my finger is more relaxed, and accessing my PC becomes painfully frustating even for those with my permission.

Cheers! :smile:

`image by Hal Gatewood`—[Unsplash](https://unsplash.com/@halgatewood)

[^1]: https://www.kaufmann.no/roland/dvorak/



