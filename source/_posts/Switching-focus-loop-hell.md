---
title: Switching focus loop hell
toc: true
thumbnail: https://images.unsplash.com/photo-1527168027773-0cc890c4f42e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60
date: 2019-11-25 09:36:41
tags:
	- Productivity
category:
	- Thought
---

I blame myself for reading _un_-productive book for this thought. But it was me the one who choose to read them, so don’t blame the book. 

<!--more-->

First, I don’t believe in multitasking. I believe in one task in one timeframe. Best people work on smallest possible time frame **for them**. I emphasize the word to make sure you all understand that my time frame is not yours. And vice versa.

And by time frame, it means _no disturbance_ to what they are doing right now. The disturbance must wait until the current timeframe ended. That’s why people like me that still can’t get focused that fast _will_ get mad when disturbed. Even if it is a life-death situation.

But we must keep in mind that our time is limited to 24 hours a day (I won’t talk about the exact amount, ask xkcd). So, after got cut by several human routine (sleep, eat, pray, sex, etc), you might got 12 hours or so.

Now, divide it by work and projects.

Comparing the timeline I have and projects I need to finish, and looking into my expertise on the field I am working on, might get me relieved or screaming, depending on the result.

I don’t deny that multi project is part of the work. You will work on multi project at the same month, not the same timeframe. The problem raised when your timeframe start to break their boundary…

You entered switching focus hell loop.

In the shortest amount of time, I need to talk to my junior about their task on project A, then explaining things to programmer at project B, while my other programmer has been waiting for me to finish my sentence to ask me about their project, which they do. 

I have experienced this kind of time, being asked for many problem within a project is fine. Multi project? As long as it is not _that_ fast.

Sometimes, I even lost, I mean, really lost. Like when you suddenly got blank and don’t know what to do. Yea.

It’s been a month and I still blank. Finally today I can slowly get back on things. Guess a burnout because of that many life matters are real.

<a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@kylejglenn?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Kyle Glenn"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Kyle Glenn</span></a>