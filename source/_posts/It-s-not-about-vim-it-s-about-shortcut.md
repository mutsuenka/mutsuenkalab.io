---
title: It's not about vim it's about shortcut
toc: true
sidebar:
  left:
    sticky: true
widgets:
  - type: toc
    position: left
date: 2019-11-23 19:24:00
tags:
	- ultimatehackingkeyboard
	- vim
categories:
	- keyboard & layout
---

I was a regular user of Vim. I started at college, and use it daily with my life as a programmer, and an IT Business Analyst. It is my main text editor. Other than Word Processor, I only use Vim to process my texts and writing.

<!--more-->

## The reason

At college, I use Vim because it was cool. Seriously. You looks geeky and nerdy when using it. The kind of impression you won’t get from using notepad++. At my college, Vim is associated with _the hardest OS ever in the world a.k.a LINUX_ (seriously guys, don’t ask why), thus, using it gives you “smart guy” choice.

After graduating and waking up from college dream, I found my self type a lot of texts. With a lot of texts, come a lot of mistakes. I am touch typing since grade school, so yes, my fingers are breaking with standard windows app. However, Vim has shortcuts. YESS, SHORTCUTS. There are layers of command that you can map. Within Normal mode, Insert mode, Visual mode, and any other mode that I might not have use them.

So, I use it, even with all the technical difficulties when trying to make it run on Windows machine. Because shortcut has more values for me than the difficulty in making it run and customizing it to fit my need.

## The Usage and Customization

I used Vim for programming and writing. While it looks simple, those two are the reasons why I configure Vim, and struggle in make it run good and stable.

As we are all know, stacking and installing all those plugins to Vim bound to an inevitable crashes. We need to patiently stacking them, checking the results, etc. Or just use other people configurations like `spf13`. 

Using Vim for programming and writing at the same time has it cost. When I need to write something real quick, notepad and notepad++ is definitely launch faster than my configured Vim. And all I want is write those notes with markdown. I’d rather have Vim open all the time than wait for it to launch in a nick of time.

Since I promoted to become an IT Business Analyst, I reduce the usage of it for programming. Moreover, I found Neovim. I switch to it, install and configures it a new, and use it heavily for writing. 

This time Neovim is really nice to use.

## Then come the UHK

All of it was nice, until I have my UltimateHackingKeyboard last year. 

Remember the reason why I was using Vim? It is because of the shortcut and commands. With ultimatehackingkeyboard, I can use the shortcut OS-Wide. Not only at Vim, but on all OS. 

And the configuration stick with the keyboard, thus, connecting the keyboard everywhere will execute the exact same command to the OS. 

I am not a macro person, I am a shortcut person. UHK can’t execute functions like Vim. But that is fine. I have researched a lot of app, and they are starting to create the function _I need_ with the shortcut _I can map_ to my UHK.

## And then Vim becomes standard app

I am not threating Vim like some app that need to be avoided. I still use it for some tasks that requires it’s function. 

Now I code most on Visual Studio Code and write most on Typora. Good enough and fully functional enough for my need. 

I still configures things using Vim. Can’t move on from it. I still miss Vim’s functions on many platform, but hey, not all app can become Vim. Just map it on the UHK, and I am good to go.

Now, as all application I installed, for me, Vim becomes an app with specific functions, not that jack of all trade app that I use for everything related to texts except print document writing. It has it glory in my life, and it has not yet fade to abyss.