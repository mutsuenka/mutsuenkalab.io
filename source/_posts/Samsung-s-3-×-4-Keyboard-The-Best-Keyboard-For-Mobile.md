---
title: 'Samsung''s 3 × 4 Keyboard, The Best Keyboard For Mobile'
toc: true
thumbnail: https://live.staticflickr.com/65535/49099365072_4188db2d59_b.jpg
sidebar:
  left:
    sticky: true
widgets:
  - type: category
    position: left
date: 2019-08-19 21:36:36
tags:
	- Samsung
	- Mobile 
category: keyboard & layout
---

Today, my smartphone is busted, so I need to do factory reset. But that event allows me to discover something great, 3 × 4 keyboard of Samsung.

<!--more-->

That 3 × 4 Keyboard has been evolved to a completely effective means to write a long narrative without even making silly typo mistakes.

Moreover, I can type by using right or left hand only! Wat leuk!

Thanks to predictive system that has been continuously evolved up to now, you can believe that this keyboard know what you want to type, we are no longer need to press one button several time to get character we want.

![](https://live.staticflickr.com/65535/49099364997_11e56f7967_b.jpg)

For example, if you want to type “requirements”, you only need to press button 7,3,7,8,4,7,3,6,3,6,8 and 7. And _Voila_! It will directly translated to “requirements”

In fact, this whole post is written using the layout I am explaining now!

If you need to type a long text in mobile and have no keyboard around you, you definitely shall give this layout a try, you definitely will feel the difference almost immediately.

However, quick predictive works onle at “text area” or “text input”. It doesn’t work on search or browser’s address bar, or any text field which requires precise input. But it’s okay, since it works on telegram.

As far as I know, only Samsung has provided this functionality, I tried AnySoftKeyboard, but the method is just like the old feature phone keypad. Swiftkey doesn’t even have it.

So, for me, this is a olution of typing a lot of narrative on my phone. Might be clumsy, but definitely worth it. If you use Samsung Device, try the feature! You can find it on: 

`Settings › Language and Input › On Screen Keyboard › Samsung keyboard › Language and Types` tap the English layout, and select 3 × 4 layout keyboard.

![](https://live.staticflickr.com/65535/49099364917_e50ffa118d_b.jpg)

Have fun trying the feature! Hope it solves your typing problem too!

—`this is an old post that I wrote while I still hosting a blog on wordpress, thus, the mobile app`