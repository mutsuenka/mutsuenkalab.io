---
title: bad rating culture is bad
toc: true
date: 2019-12-05 23:45:34
tags:
	- Rambling
category:
	- Thought
thumbnail: https://images.unsplash.com/photo-1509720756543-39ce267344f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80
---

Since I’ve known the internet, I’ve known the rating, or likes. You know, something to show how valuable things are. For social media, it is most likely using a simple like button, for services or goods, it is most likely using 5 star rating.

<!--more-->

5 star rating  is a good way, however, the mindset of people who are voting using five star rating is definitely bad. IMHO, a 4 star rating is still a good rate for a service or an app that I enjoy, just like that. 5 star is a golden rating value that makes me think of giving tips for services, or revisit the item again for goods.

But people are being obsessed by 5 star rating. Standard services, 5 star demands. Bad attitude, 5 star. Giving 4 or 3 star considered a crime in a world full of people that will eat you alive by telling them they shall not phubing when driving, or smoking when riding.

Across online services here in Indonesia, I see people only understand that there are two ratings: 5 star for acceptable and good service, 1 star for bad or bad-for-me services. 2, 3 and 4 stars are most likely don’t exist. Anything other than 5 are considered bad rating. Even though I just use my head and rate you according to the services or goods you provide me.

I don’t know, maybe this thing has relation with the praise hungry culture that drove by positivism? Wait, is that even -ism now?

I am okay with getting 3 star rating for a standard routine service I gave people. If I don’t please you, deal with it. It might be me being lazy or you have your high standard. 3 means my service is still acceptable, but just… bland. Bland services and bland apps are not bad, just enough. But that’s the point. Nothing good is just enough.

Ah, we might just not ready enough to let other people judge our services and products, difference in standard do play a part on injustice rating, such as 1 star rating because of hatred or envy. Understanding shall play a big part here. With rating is 5–1, it is not weird to get a service that overall rating only stand on 4. It is still a good rating. some people really satisfied with it, some people get a good impression, but demand on something more. See? Ez isn’t it?

Ah, I don’t understand why am I even rambling about it. But hey. This is gitlab page. At least my contribution points went up.

<a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@azganmjeshtri?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from AZGAN MjESHTRI"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">AZGAN MjESHTRI</span></a>