---
title: Analis pun harus bisa jaringan
date: 2019-12-29 14:54:22
tags:
	- Rambling
	- Personal Project
	- Professional
category:
	- Analyst Life
thumbnail: https://images.unsplash.com/photo-1551703599-6b3e8379aa8c?ixlib=rb-1.2.1&auto=format&fit=crop&w=1352&q=80
---
Di weekend ini, saya ada uprekan baru. Semua developer saya sudah biasa menggunakan Amazon Web Service. Me? Nope. Saya kan biasanya terima jadi saja. Namanya juga analis. Tapi, masalah muncul ketika anda bekerja freelance bareng-bareng teman lain dan anda yang paling ngerti masalah produk yang digunakan. 

<!--more-->

Paling ngerti di sini bukan berarti anda sudah jago. Tapi, yang lain betul-betul _clueless_ as to ini AWS makenya gimana sih… Bener-bener tepok jidat sih, ya masalahnya teman banyaknya mengerjakan proyek yang memang harus on premise, ndak bisa cloud computing, jadi mana pernah doi make AWS atau Azure. 

Secara saya pernah ngutak-atik lightsail buat blog saya yang wordpress dulu, akhirnya saya coba bikin-bikin instance AWS buat masalah di atas. 

_Turns out_ bikin EC2 instance gampang juga. Saya masih ga mikirin bill karena, «free tier». So, saya buat EC2 instance, generate key, buat IAM account. Terus saya kasihkan ke temen saya.

Done? Nope.

> Koneksi gw direject cuy
>
> <p style="text-align:right;">—Temen Dev</p>

Aisssh. Saya langsung coba akses dengan akses key tersebut via PuTTy dan boot linux saya untuk ngecek akses via bash terminal biasa. Done. Connected. Saya infokan ke temen. Dia ga percaya. Dia coba lagi.

Ternyata emang dari sononya port 22 diblok ama providernya dia. Kan lutjuk.

Oké, yang penting bisa konek dulu deh. Temen Dev saya langsung proses deploy, cuma dev sih. Dalam waktu sejam, semuanya beres. “Udah running semua oom.” Oké, karena saya biasa di lightsail main asal konek ke IPnya aja, saya langsung akses + port yang diperlukan.

> Error 502: Bad Gateway

Wat?

Oké, saya ga ngerti jaringan, jadi mumet, temen ga pernah pake AWS, jadi ga ngerti, karena ketika dia remote ngecek, seluruh aplikasi jalan dengan normal. Thus, saya coba arahkan ke domain saya yang lagi parkir.

Sudah utak atik route 53 segitu lama. Ternyata tetap gagal juga.

How? Hahaha saya cuma bisa ketawa. 

Akhirnya saya dan teman-teman sewa VPS di jagoanhosting. Dan dalam waktu setengah jam itu proyek bisa “live” versi devnya, buat UAT user.

EC2 yang saya buat? ya diterminate. jangan ampe masuk cycle billing lah.

Mungkin kasus ini ga ada kaitannya ama sekali dengan saya yang seorang analis, tapi kasus ini mengingatkan saya bahwa, Analis tetep IT Guy, dan IT Guy yang cuma bisa ngomong depan klien itu kayak biker yang cuma bisa jualan dan ngomentarin motornya. Ga nyambung sih, tapi biarin aja.

Intinya, as a patial IT Guy, paling ga anda bisa lah nyetting-nyetting sesuatu.

<p style="text-align:right;">29 Desember 2019<br />—Muhammad</p>
